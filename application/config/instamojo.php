<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* instamojo payment API v1 library for CodeIgniter
*
* @license Creative Commons Attribution 3.0 <http://creativecommons.org/licenses/by/3.0/>
* @version 1.0
*/

$config['mojo_mode']  = 'sandbox' ;
/*
|--------------------------------------------------------------------------
| API_KEY
|--------------------------------------------------------------------------
| API_KEY are different for test and production !
| $config['mojo_apikey'] = '650f7eed3d900273d6fafd635a';
|
*/
//$config['mojo_apikey'] = 'ed223e32d75755c9b9feabd4d40a4dcf' ;
$config['mojo_apikey'] = 'test_b967a916ab2f553cb6abf5336bc';
/*
|--------------------------------------------------------------------------
| AUTH_TOKEN
|--------------------------------------------------------------------------
| AUTH_TOKEN are different for test and production !
| $config['mojo_token'] = '650f7eed3d900273d6fafd635a';
|
*/
//$config['mojo_token']  = 'b5bffc91b7ce9366bec59fdb0fece7e9' ;
$config['mojo_token']  ='test_33e091c643815ffee8ae98950de';
/*
|--------------------------------------------------------------------------
| REDIRECT_URL
|--------------------------------------------------------------------------
| Set redirect url !
| $config['mojo_url'] = 'https://github.com/Instamojo/instamojo-php';
|
*/
$config['mojo_url'] = 'http://bml.3esofttech.com/PayuInstamojo/success';


$config['mojo_webhook']="http://bml.3esofttech.com/PayuInstamojo/webhook";
