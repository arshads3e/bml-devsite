<?php

function bml_urlencode($content)
{
  $content = str_replace("/","'",$content);
  $content = urlencode($content);
  return $content;
}

function bml_urldecode($content)
{
  $content = urldecode($content);
  $content = str_replace("'","/",$content);
  return $content;
}

function admin_url($url)
{
  return get_instance()->config->item('admin_url').$url;
}

function upload_url($url)
{
  $upload_url = get_instance()->config->item('upload_path').$url;
  return str_replace('\\','/',$upload_url);
}
?>
