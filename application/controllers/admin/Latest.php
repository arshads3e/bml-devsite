<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Latest extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  }
  public function getLatestDetails()
  {
    $data = array("title" => "Products ", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getLatestDetails();
    $itemResult=$this->adminModel->getProductDetailsBySatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();
    
    $data['tableRows'] = $result;
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $data['itemResult'] = $itemResult;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/latest');
    $this->load->view('admin/footer');
  }
  public function removeLatest($id)
  {
    $this->adminModel->removeLatest($id);
    header('Location: '.$this->agent->referrer());
  }
  public function insertLatestItem()
  {
    $itemID=$this->input->post('itemID');
    $this->adminModel->insertLatestItem($itemID);
    header('Location: '.$this->agent->referrer()); 
  }
}