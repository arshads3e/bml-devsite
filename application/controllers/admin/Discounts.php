<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discounts extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  }
  public function getDiscountDetails()
  {
    $data = array("title" => "Discount Details", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getDiscountDetails();
    $result = (array_key_exists(0, $result))? $result[0]: array();
    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/viewDiscount');
    $this->load->view('admin/footer');
  }
  public function editDiscountDetails($id)
  {
    $data = array("title" => "Discount Details", "subTitle" => $id."","sidebarCollapse" => true);
    $result = $this->adminModel->viewDiscountDetailsByID($id);
    $result1 = $this->adminModel->getCustomer();

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $data['tableRows'] = $result;
    $result1 = (array_key_exists(0, $result1))? $result1[0]: array();
    $data['tableRows1'] = $result1;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/editDiscountDetails');
    $this->load->view('admin/footer');
  }
  public function addDiscount()
  {
    $data = array("title" => "Add Discount Details", "subTitle" =>"","sidebarCollapse" => true);

    $result1 = $this->adminModel->getCustomer();
    $result1 = (array_key_exists(0, $result1))? $result1[0]: array();
    $data['tableRows1'] = $result1;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/addDiscount');
    $this->load->view('admin/footer');
  }
  public function updateDiscountDetails()
  {
    $Id=$this->input->post('Id');
    $discount=$this->input->post('discount');
    $discountCount=$this->input->post('discountCount');
    $promocode=$this->input->post('promocode');
    $status=$this->input->post('status');
    $customerIds=implode(',', $_POST['customerIds']);
    $this->adminModel->updateDiscountDetails($Id,$discount,$discountCount,$promocode,$customerIds,$status);
    header('Location: '.$this->agent->referrer());
  }
  public function insertDiscountDetails()
  {

    $discount=$this->input->post('discount');
    $discountCount=$this->input->post('discountCount');
    $promocode=$this->input->post('promocode');
    $status=$this->input->post('status');
    $customerIds=implode(',', $_POST['customerIds']);
    $this->adminModel->insertDiscountDetails($discount,$discountCount,$promocode,$customerIds,$status);
    //redirect(admin_url('discounts/getDiscountDetails/'));
  }

}
