<div class="clearfix"></div>
<br>
<div class="col-sm-1 col-md-1"></div>
<div class="col-sm-10 col-md-10">
  <div class="row">
    <div class="col-md-8">
      <?php
        $breadcrumbText = '';
        foreach ($breadcrumb as $breadcrumbRow){
          $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
          $breadcrumbText .= " > ";
        }
        echo rtrim($breadcrumbText, " > ");
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 table-responsive">

      <h2>Order Summary</h2>
	  <?php if(isset($txnFailed)) { ?>
	  <p class="text-danger"><b>IMPORTANT : </b>Your Payment has failed. Our admin team will contact you.</p>
      <?php } ?>
	  
	  <table class="table">
        <thead class="background-color-c1272d color-fff">
          <th>Order Number</th>
          <th>Product Name</th>
          <th>Pickup Date</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Return Date</th>
          <th>Qty</th>
          <th>Price</th>
          <th>Sub-Total</th>
        </thead>
        <tbody>
        <?php foreach ($subOrderItems as $orderNumber => $orderItems) { $orderItemCount = count($orderItems);?>
          <tr class="border-top-2px-solid-C1272D" >
            <td rowspan="<?php echo $orderItemCount;?>" class="vertical-align-middle border-left-2px-solid-C1272D border-right-2px-solid-C1272D"><?php echo $orderNumber;?></td>
            <?php
              $subOrderTotal = 0;
              $first = true;
              $colTextArr = "";
              foreach ($orderItems as $orderItem) {
                $waitingList = ($orderItem['status'] == 6) ? "class='bg-danger' title='waitingList'":'';
                $colText = "";
                $subOrderTotal += $orderItem['rentalTotalPrice'];
                if(!$first) $colText.="</tr><tr>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['itemName']."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['collectDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['startDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['endDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['returnDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['qty']."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['rentalTotalPrice']."</td>";
                $colTextArr[] = $colText;
                $first = false;
              }
              $first = true;
              foreach ($colTextArr as $colText) {
                echo $colText;
                if($first) {
                  echo "<td rowspan='".$orderItemCount."' class='vertical-align-middle border-left-2px-solid-C1272D border-right-2px-solid-C1272D'>
                            ".$subOrderTotal."
                        </td>";
                  $first = false;
                }
              }
            ?>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="clearfx"></div>

  <?php if(empty($pickupAddr)) { ?>
  <div class="col-md-4 padding-0px font-weight-200">
    <h4  class="font-size-13px line-height-36px background-color-c1272d padding-left-15px color-fff"><b>Pickup Address<b></h4>
    <div class="col-md-4"><b>Address :</b></div>
    <div class="col-md-8 font-weight-200"><?php echo $pickupAddr['addressLine1']; ?><br><?php echo $pickupAddr['addressLine2']; ?>,</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"><?php echo $pickupAddr['addressCity']; ?>,</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"><?php echo $pickupAddr['addressState']; ?> - <?php echo $pickupAddr['addressPin']; ?>.</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"></div>
    <div class="clearfix"></div>
    <div class="col-md-4"><b>Landmark :</b></div>
    <div class="col-md-8 font-weight-200"><?php echo $pickupAddr['addressLandmark']; ?>.</div>
  </div>
<?php } ?>
<?php if(empty($returnAddr)) {?>
  <div class="col-md-4" >
    <h4  class="font-size-13px line-height-36px background-color-c1272d padding-left-15px color-fff"><b>Return Address</b></h4>
    <div class="col-md-4">Address :</b></div>
    <div class="col-md-8 font-weight-200"><?php echo $returnAddr['addressLine1']; ?><br><?php echo $returnAddr['addressLine2']; ?>,</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"><?php echo $returnAddr['addressCity']; ?>,</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"><?php echo $returnAddr['addressState']; ?> - <?php echo $returnAddr['addressPin']; ?>.</div>
    <div class="clearfix"></div>
    <div class="col-md-4"></div>
    <div class="col-md-8 font-weight-200"></div>
    <div class="clearfix"></div>
    <div class="col-md-4"><b>Landmark :</b></div>
    <div class="col-md-8 font-weight-200"><?php echo $returnAddr['addressLandmark']; ?></div>
  </div>
<?php } ?>

  <div class="col-md-4 padding-0px">
    <h4 class="font-size-13px line-height-36px background-color-c1272d padding-left-15px color-fff"
  ><b>Order Details</b></h4>
    <div class="col-md-8">
      <b>Sub Total :</b>
      <div class="clearfix"></div>
      <span class="text-danger font-size-10px">(excluding products in waitingList)</span>
    </div>
    <div class="col-md-4 text-right" ><span class="text-color"><i class="fa fa-inr"></i></span> <?php echo $this->cart->format_number(round(($total+$dicountAmount))); ?>  </div>
    <div class="clearfix"></div>
    <hr class="padding-3px-px margin-0px">

    <div class="col-md-8">
      <b>Discounts :</b>
    </div>
    <div class="col-md-4 text-right" ><span class="text-color"><i class="fa fa-inr"></i></span>  <?php echo $this->cart->format_number($dicountAmount); ?> </div>
    <div class="clearfix"></div>

    <div class="col-md-8">
      <b>Shipping Cost :</b>
    </div>
    <div class="col-md-4 text-right" ><span class="text-color"><i class="fa fa-inr"></i></span>  <?php echo $this->cart->format_number($shippingCost); ?> </div>
    <div class="clearfix"></div>

    <hr class="padding-3px-0px margin-5px-0px border-top-1px-solid-c1272d">
    <div class="col-md-8 text-success">
      <b class="font-size-18px" >Grand Total :</b><br>
      <span class="text-danger font-size-10px" >(excluding products in waitingList & shipping)</span>
    </div>
    <div class="col-md-4 text-success text-right" ><span class="text-color"><i class="fa fa-inr"></i></span><b class="font-size-16px"> <?php echo $this->cart->format_number($total); ?>  </b></div>
    <div class="clearfix"></div>

  </div>
  <div class="clearfix"></div>
  <hr>
  <div>
    <p>
      <b class="text-danger font-size-18px">IMPORTANT :</b>
    </p>
    <div class="col-lg-12">
    <ul class="font-weight-200 font-size-13px">
        
		<?php if(isset($pumAmt)) { ?>
			<li>Amount Recieved : Rs. <?=$pumAmt;?></li>
		<?php } else if(isset($txnFailed)) { ?>
			<li>Your Payment has failed</li>
		<?php } else { ?>
	  <li>
          Please make 1 day rental/full amount as advance payment in next 48hrs to confirm the same.
      </li>
      <li>Please transfer the advance amount to </li>
      
        <div class="col-md-2">NAME : </div>
        <div class="col-md-10"><b>Creative Capture Pvt Ltd (BOOK MY LENS)</b></div>
        <div class="clearfix"></div>
        <div class="col-md-2">BANK : </div>
        <div class="col-md-10"><b>HDFC Bank , KORAMANGALA</b></div>
        <div class="clearfix"></div>
        <div class="col-md-2">IFSC code : </div>
        <div class="col-md-10"><b>HDFC0000053</b></div>
        <div class="clearfix"></div>
        <div class="col-md-2">Current A/C #.: </div>
        <div class="col-md-10"><b>50200005019560</b></div>
        <div class="clearfix"></div>
      <?php } ?>
      <li>
        Please note, for the items on waiting list, we will intimate you by email to confirm your booking depending on the availability of the chosen item. Upon confirmation you can proceed with the advance payment. For any assitance please contact us on phone or drop an email.
      </li>
    </ul>
  </div>
  </div>
</div>
<div class="col-sm-1 col-md-1"></div>

<div class="col-sm-12 col-md-12" style="text-align:center">
<form action="<?php echo site_url();?>">
<input type="submit" class="button font-weight-bold font-size-18px color-EAD608  height-40px padding-top-8px" style="background: #C1272D;" value="Back to Home" onclick="return checkWaiting();">
</form>
</div>
