          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('products/insertHomeSlideDetails') ;?>" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    
                      <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Title :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='text' name='slidetitle'  class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Content :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <textarea type='text' name='slidecontent' rows='3' class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide BG-Image :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='file' name='slideBGImage'  class="form-control" />
                        </div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Item Image:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='file' name='slideItemImage'  class="form-control" />
                        </div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Item Name:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input require type="text" id="itemAutoComplete" class="form-control"  name="slideItemID" /> 
                            
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <hr>
                      <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <button class="btn btn-warning" type="reset">RESET</button> </h2>
                   </form>
                   <div class="clearfix"></div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>
      <script type="text/javascript">
      itemList = <?php echo json_encode($itemList);?>;
        $( "#itemAutoComplete" ).autocomplete(
          {
            source:itemList
          });

        function custom_source_client(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(clientList, function(value) {
                return matcher.test(value.label)
                        || matcher.test(value.number)
                          || matcher.test(value.email);
            }));
        }
      </script>


     



