<div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('discounts/insertDiscountDetails') ;?>" method="post">
                   
                  <div class="col-lg-4 col-sm-4">
                        <div class="col-lg-6 col-sm-6 text-right">
                          <b>Promocode:</b>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                          <input type="text" name='promocode' value=''  class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-6 col-sm-6 text-right padding-top1">
                          <b>Discount :</b>
                        </div>
                        <div class="col-lg-6 col-sm-6 padding-top1">
                          <input type="text" name='discount' value='' class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-6 col-sm-6 text-right padding-top1">
                          <b>Discount Count:</b>
                        </div>
                        <div class="col-lg-6 col-sm-6 padding-top1">
                          <input type="text" name='discountCount' value='' class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        
                        
                  </div>

                   <div class="col-lg-4">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b> Status :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='status'>
                            <option value='-1'>Select</option>
                            <option value='1'>Active</option>
                            <option value='0'>InActive</option>
                            
                          </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Brand Name :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='customerIds[]' multiple>
                            <option value='-1'>Select</option>
                          <?php foreach ($tableRows1 as $customer) { ?>
                                <option value="<?php echo $customer['customerId']; ?>">
                                    <?php echo $customer['customerNumber']; ?> - <?php echo $customer['firstName']; ?>
                                </option>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                   </div>
                   <div class="clearfix"></div>
                   <hr>
                   <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <a href="<?php echo admin_url('products/getDiscountDetails'); ?>" class="btn btn-warning" >CANCEL</a></h2>
                   </form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>