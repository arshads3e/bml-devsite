<div class="box">
  <div class="box-body">
    <div class="col-lg-12">
    <form class="form-horizontal" role="form"  action="<?php echo site_url('myaccount/addPayment');?>" method="post">
      <div class="form-group">
        <label class="control-label col-sm-4" for="orderSelect">OrderNumbers:</label>
        <div class="col-sm-8">
          <select required name="paymentOrderID" id="orderSelect" class="form-control" >
            <?php echo $orderNumbersOption ;?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="paymentType">Payment Type:</label>
        <div class="col-sm-8">
          <select required name="paymentTypeID" id="paymentType" class="form-control" >
            <?php echo $paymentOptions;?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="bankName">Bank Name:</label>
        <div class="col-sm-8">
          <input required type="text" class="form-control" id="bankName" name ="bankName" placeholder="Bank Name">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="branchName">Branch Name:</label>
        <div class="col-sm-8">
          <input type="text" required class="form-control" id="branchName" name ="branchName" placeholder="Branch Name">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="refNum">Reference #:</label>
        <div class="col-sm-8">
          <input required type="text" class="form-control" id="refNum" name ="refNum" placeholder="">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="amount">Amount:</label>
        <div class="col-sm-8">
          <input required type="text" class="form-control" id="amount" name ="amount" placeholder="Amount Paid">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="date">Date:</label>
        <div class="col-sm-8">
          <input required type="text" class="form-control" id="date" name ="date">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-4 col-sm-2">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
        <div class="col-sm-2">
          <button type="reset" class="btn btn-danger">Reset</button>
        </div>
        <div class="col-sm-2">
          <button type="button" onclick="cloasForm();" class="btn btn-info">Close</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
