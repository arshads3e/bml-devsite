          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <table id="clientList" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <?php foreach ($tableHeader as $headerItem) {?>
                          <th><?php echo $headerItem;?></th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tableRows as $tableRow) {?>
                      <tr>
                        <?php foreach ($tableRow as $col) {?>
                          <td><?php echo $col;?></td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) {
    $('#clientList').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
	  "iDisplayLength": 50,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false,
      columnDefs: [ { orderable: false, targets: [0] }],
    });
  });
</script>
