<div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('discounts/updateDiscountDetails') ;?>" method="post">

                  <div class="col-lg-4 col-sm-4">
                        <div class="col-lg-6 col-sm-6 text-right">
                          <b>Promocode:</b>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                          <?php $id=$tableRows[0]['discountId']; ?>
                           <input type="hidden" name='Id' value='<?php echo $tableRows[0]['discountId'];?>'>
                          <input type="text" name='promocode' value='<?php echo $tableRows[0]['promocode'];?>' readonly class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-6 col-sm-6 text-right padding-top1">
                          <b>Discount :</b>
                        </div>
                        <div class="col-lg-6 col-sm-6 padding-top1">
                          <input type="text" name='discount' value='<?php echo $tableRows[0]['discount'];?>' class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-6 col-sm-6 text-right padding-top1">
                          <b>Discount Count:</b>
                        </div>
                        <div class="col-lg-6 col-sm-6 padding-top1">
                          <input type="text" name='discountCount' value='<?php echo $tableRows[0]['discountCount'];?>' class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-6 col-sm-6 text-right padding-top1">
                          <b> Status :</b>
                        </div>
                        <div class="col-lg-6 col-sm-6 padding-top1">
                          <select class="form-control" name='status'>
                            <?php if($tableRows[0]['status']==1){?>
                            <option value='1' selected>Active</option>
                            <option value='0'>InActive</option>
                            <?php } else {?>
                            <option value='1' >Active</option>
                            <option value='0' selected>InActive</option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                  </div>

                   <div class="col-lg-4">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Customer Name :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='customerIds[]' multiple>
                          <?php foreach ($tableRows1 as $customer) { ?>
                          <?php $customerIds=explode(',', $tableRows[0]['customerIds']); ?>
                              <?php $selected="";
                                foreach ($customerIds as $custID){ ?>
                                  <?php
                                    if($customer['customerId']== $custID)
                                      {
                                        $selected='selected';
                                      }
                                  }
                              ?>
                                <option value="<?php echo $customer['customerId']; ?>" <?php echo $selected; ?>>
                                    <?php echo $customer['customerNumber']; ?> - <?php echo $customer['firstName']; ?>
                                </option>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                   </div>
                   <div class="clearfix"></div>
                   <hr>
                   <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <a href="<?php echo admin_url('products/getDiscountDetails'); ?>" class="btn btn-warning" >CANCEL</a></h2>
                   </form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>
