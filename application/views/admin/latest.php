 <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <div class="col-lg-12">
                    <form class="form-horizontal" action="<?php echo admin_url('Products/insertLatestItem')?>" method="post">
                      <div class="ui-widget">

                        <div class="col-lg-4">
                          <select id="combobox" class="form-control" name="itemID">
                            <?php foreach ($itemResult as $row) { ?>
                            <option value="<?php echo $row['itemId']; ?>"><?php echo $row['itemName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-lg-3">
                          <button class="btn btn-primary" type="submit"> Add to Latest</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="clearfix"></div>
                  <table id="latestDetails" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td>Latest ID</td>
                        <td>Item Name</td>
                        <td>Item Type</td>
                        <td>Category Name</td>
                        <td>Brand Name</td>
                        <td>Stocks</td>
                        <td>Item Status</td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach ($tableRows as $col) {  ?>
                        <tr>

                          <?php foreach ($itemResult as $row) { ?>

                          <?php if($col['itemId']==$row['itemId'])
                          { $id= $col['latest_id'];?>
                            <td><?php echo $col['latest_id'];?></td>
                            <td><?php echo $row['itemName'] ;?></td>
                            <td><?php echo $row['itemTypeName'] ;?></td>
                            <td><?php echo $row['category_Name'] ;?></td>
                            <td><?php echo $row['itemBrandName'] ;?></td>
                            <td><?php echo $row['numStock'] ;?></td>
                            <td><?php if($row['itemStatus']=='1') {echo "Active" ; } else { echo "InActive" ;  }?></td>
                            <td><a href="<?php echo admin_url('Products/removeLatest/'.$id); ?>" class="btn btn-warning btn-xs">Remove</a></td>



                          <?php } ?>
                          <?php } ?>
                          </tr>

                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>
      <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
          $('#latestDetails').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
      </script>


