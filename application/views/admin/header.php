<?php
$class = $this->router->fetch_class();
$method = $this->router->fetch_method();

$menu = [];
$menu['clients'] = array('header' => 'Manage Clients', 'submenu' => array(
																		array('method' => 'approved', 'header' => 'Approved'),
																		array('method' => 'unapproved', 'header' => 'UnApproved'),
																		array('method' => 'unapprovedall', 'header' => 'UnApproved All'),
																		array('method' => 'changeapprove', 'header' => 'Change Approval'),
																		array('method' => 'blocked', 'header' => 'Blocked')
																		));
$menu['orders'] = array('header' => 'Manage Orders', 'submenu' => array(
                                    array('method' => 'newOrder', 'header' =>'New'),
                                    array('method' => 'quotation', 'header' =>'Quotation'),
																		array('method' => 'booked', 'header' =>'Booked'),
																		array('method' => 'approved', 'header' =>'Approved'),
																		array('method' => 'onrent', 'header' =>'On Rent'),
																		array('method' => 'returned', 'header' =>'Returned'),
																		array('method' => 'completed', 'header' =>'Completed'),
																		array('method' => 'reserved', 'header' =>'Reserved'),
                                    array('method' => 'waiting', 'header' =>'Waiting'),
																		array('method' => 'cancel', 'header' =>'Cancel')
																	));
$menu['products'] = array('header' => 'Manage Products', 'submenu' => array(
									array('method' => 'avail', 'header' => 'Availability'),
																		array('method' => 'addProduct', 'header' =>'Add Product'),
																		array('method' => 'getProductDetails', 'header' =>'View Products'),
																		array('method' => 'getFeaturedDetails', 'header' =>'Featured Products'),
                                    array('method' => 'getLatestDetails', 'header' => 'View Latest'),
									array('method' => 'getHomeSliderDetails', 'header' => 'View Home Slider'),
                                    array('method' => 'addHomeSliderDetails', 'header' => 'Add Home Slider')
																	));
$menu['discounts'] = array('header' => 'Manage Discounts', 'submenu' => array(
																			array('method' => 'addDiscount', 'header' =>'Add Discounts'),
																			array('method' => 'getDiscountDetails', 'header' => 'View Discounts')
																		));
$menu['payments'] = array('header' => 'Manage Payments', 'submenu' => array(
                                      array('method' => 'addNew', 'header' =>'Add Payment'),
                                      array('method' => 'view', 'header' => 'View Payment')
                                    ));


?>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo admin_url('home');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BML</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BML</b> Admin Panel</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo site_url('myaccount/logout');?>" class="btn-flat">Sign out</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php foreach ($menu as $className => $menuRow) {?>
        <?php $activeMain = ($className == $class)?'active':'';?>
        <li class="<?php echo $activeMain;?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i>
            <span><?php echo $menuRow['header'];?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
          	<?php foreach ($menuRow['submenu'] as $submenu) {?>
          	<?php $active = ($activeMain != '' && $submenu['method'] == $method)?'active':'';?>
            	<li class="<?php echo $active; ?>">
            		<a href="<?php echo admin_url($className.'/'.$submenu['method'])?>">
            			<i class="fa fa-circle-o"></i>
            			<?php echo $submenu['header'];?>
            		</a>
            	</li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper min-height-500px" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subTitle;?></small>
      </h1>

    </section>
