          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <table id="productDetails" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td>Item ID</td>
                        <td>Item Name</td>
                        <td>Item Type</td>
                        <td>Category Name</td>
                        <td>Brand Name</td>
                        <td>Stocks</td>
                        <td>Item Status</td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($tableRows as $row) { ?>
                        <tr>
                          <?php foreach ($row as $colName=>$cols) { ?>
                          <td>
                           <?php if($colName=='itemStatus') {?>
                           <?php 
                             if($cols==1)
                             {
                              echo "ACTIVE";
                             }
                             else
                             {
                              echo "INACTIVE";
                             }

                           ?>
                           <?php } elseif ($colName=='numStock') { ?>
                             <?php echo "<div class='margin-left-25 margin-right-25' >$cols</div>";?>
                             <?php } elseif ($colName=='itemId') { ?> 
                             <?php  $id=$cols; 
                                    echo $cols;
                                  } else { ?>                        
                              <?php echo $cols;?>
                              <?php } ?>
                            </td>
                          <?php } ?>
                          <td><a href="<?php echo admin_url('products/viewProductDetails/'.$id); ?>" class="btn btn-success btn-xs">EDIT</a></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
          $('#productDetails').dataTable({
            "bPaginate": true,
			"bLengthChange": true,
			"iDisplayLength": 50,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
      </script>
