<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AppModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function validateToken($token, $user) {
		$sql ="SELECT ifnull(a.`isLogout`,'-1') as isLogout, userId, b.status as status
				FROM `tbl_app_login` a, customer b 
				WHERE a.`userId` = b.`customerId`
				and b.`emailId` = '$user'
				and a.`token` = '$token'";
		$res = $this->bml_database->getResults($sql);
		$login = (isset($res[0][0]['isLogout']) && $res[0][0]['isLogout'] == 0)? $res[0][0]['userId']: -1;
		$status = (isset($res[0][0]['status'])) ? $res[0][0]['status'] : 0;
		return array('login' => $login, 'status' => $status); 
	}
	
	public function logout($token, $user)
	{
		$sql = "update `tbl_app_login` 
				set `isLogout` = 1
				and `dateLogout` = now()
				WHERE `token` = '$token'";
		$this->bml_database->getResults($sql);
		return true;
	}
	
	public function getCategories() {
		$sql = "SELECT * FROM  `category_tb` ORDER BY  `category_tb`.`displayOrder` ASC ";
		$res = $this->bml_database->getResults($sql);
		return (isset($res[0])) ? $res[0] : array();
	}
	
	public function getProductDetailsById($id) {
		$sql = "SELECT a.`itemId`, b.itemBrandName, c.category_Name, a.`itemDescription`, a.`itemImage2` as 'itemImage', a.`itemName`, a.`seasonID`, a.`itemSpecification`
            FROM `itemmaster` a
            JOIN tbl_item_brand b
            on a.`itemBrand` = b.itemBrandID
            JOIN category_tb c
            ON a.`itemCategory` = c.categoryID
            WHERE a.`itemId` = '$id'
            and a.`itemStatus` = 1";

		$resultSet = $this->bml_database->getResults($sql);
		$data = (isset($resultSet[0][0])) ? $resultSet[0][0]: array();
		
		if(!empty($data))
		{
			$sql = "SELECT `days`, `price` FROM `pricemaster` WHERE `itemId` = ".$data['itemId']." and `seasonId` = ".$data['seasonID'];
			$resultSet = $this->bml_database->getResults($sql);
			$data['priceList'] = (isset($resultSet[0])) ? $resultSet[0]: array();
		}
		
		return $data;
	}
	
	public function getProductsByCategoryId($id,$type) {
		
		if($type == 0)
		{
			$sql = "SELECT `categoryNames` as 'category_Name' FROM `tbl_main_page_category` WHERE `mainpageCategoryId` = '$id'";
		}
		else
		{
			$sql = "SELECT `category_Name` as 'category_Name' FROM `category_tb` WHERE `categoryID` = '$id'";
		}
		$res = $this->bml_database->getResults($sql);
		$categoryNames = ($res && isset($res[0][0]['category_Name'])) ? $res[0][0]['category_Name'] : '';
		if($categoryNames == '')
		{
			return array('categoryName' => $categoryNames, 'list' => array());
		}
		$categoryNames = str_replace(';',',',$categoryNames);
		$CI =& get_instance();
		$CI->load->model('productModel');
		$resultSet = $CI->productModel->getProductListByCategoryName($categoryNames);
		$data = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
		//get price for each product.
		foreach ($data as $key => $productlistRow) {
		  $resultSet = $CI->productModel->getPriceByProductIDAndSeasonID($productlistRow['itemId'], $productlistRow['seasonID']);
		  $data[$key]['price'] = 0;
		  $pricelistArr = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
		  foreach($pricelistArr as $priceRow)
		  {
			if($priceRow['days'] == 1) {
				$data[$key]['price'] = $priceRow['price'];
				break;
			}
		  }
		  $data[$key]['priceList'] = $pricelistArr;
		}
		return array('categoryName' => $categoryNames, 'list' => $data);
	}
	
	public function checkLogin($user, $pass)
	{
		$CI =& get_instance();
		$CI->load->model('myaccountModel');
		return $res = $CI->myaccountModel->validateLogin($user, $pass);
	}
	
	public function login($user, $pass)
	{
		$CI =& get_instance();
		$CI->load->model('myaccountModel');
		$res = $CI->myaccountModel->validateLogin($user, $pass);
		if($res)
		{
			$sql = "call sp_generate_token('$user')";
			$result = $this->bml_database->getResults($sql);
			$details=$this->myaccountModel->getUserSessionDetails();
			
			if(isset($result[0][0]['Token']) && $result[0][0]['Token'] != 0) {
				echo json_encode(array('status' => true, 'token' => $result[0][0]['Token'], 'acc_status' => $details['status']));
				return;
			}
		}
		echo json_encode(array('status' => false, 'error' => 'User/Pass Mismatch'));
	}
}