<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CommonModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getBrandsForAllCategory() {
      $sql="SELECT b.category_Name as category, c.itemBrandName as brand
            FROM `itemmaster` a, category_tb b, tbl_item_brand c
            where a.`itemCategory` = b.categoryID
            and a.`itemBrand` = c.itemBrandID
            group by `itemBrand`, `itemCategory`
            order by b.displayOrder";
      return $this->bml_database->getResults($sql);
    }

    public function getFAQs() {
      $sql="SELECT `faqquestion`, `faqanswer` FROM `faq` order by `faqid`";
      return $this->bml_database->getResults($sql);
    }

    public function getMainPageCategories()
    {
      $sql = "SELECT `mainpageCategoryId`, `categoryTitle`, `categorySubTitle`, `top5Products`, `categoryNames`, `categoryImage`, `categoryThumbImage`, `appbgcolor`, `bgcolor`, `width`
              FROM `tbl_main_page_category`
              order by `displayOrder`";
      return $this->bml_database->getResults($sql);
    }

    public function getLatestDetails()
    {
      $sql="SELECT * FROM `tbl_latest`";
      return $this->bml_database->getResults($sql);
    }
    public function getProductDetailsBySatusActive()
    {
      $sql = "SELECT a.itemId,a.`itemName`,a.`itemImage1` as itemImage,b.`itemTypeName`,d.`category_Name`,c.`itemBrandName`,  a.`numStock`,a.`itemStatus`
              FROM `itemmaster` a
              JOIN tbl_item_type b
              on a.`itemType` = b.itemTypeID
              JOIN tbl_item_brand c
              on a.`itemBrand` = c.itemBrandID
              JOIN category_tb d
              on a.`itemCategory` = d.categoryID
              WHERE a.`itemStatus`='1' ";

      return $this->bml_database->getResults($sql);
    }

    public function getHomeSliderDetails()
    {
      $sql="SELECT * FROM `homeslidercontent`";
      return $this->bml_database->getResults($sql);
    }
    public function getAllLocationNames(){
      $sql="SELECT * FROM `location`";
      return $this->bml_database->getResults($sql);
    }

    public function getLocationName($locationKey){
     
      $sql="SELECT `locationName` FROM `location` WHERE `locationKey`='".$locationKey."'";
      return $this->bml_database->getResults($sql);
      
    }

}





?>
