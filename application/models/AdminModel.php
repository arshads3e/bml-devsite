<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getAllProducts()
    {
      $sql ="select * from itemmaster";
      return $this->bml_database->getResults($sql);
    }
    public function getClientsByStatus($status, $columns = "*", $withAttachments = false)
    {
      $sql = "select $columns from customer where status = $status";
	  if($withAttachments) 
	  {
		$sql .= " and (additionalFile1 != '' || additionalFile2 != '' || addressProof != '' || idProof != '' || customerImagePath != '')";
	  }
      return $this->bml_database->getResults($sql);
    }

        public function getPaymentDetails()
    {
      $customerId = $this->user_session->getSessionVar('customerId');
      $sql = "SELECT a.*, d.typeDisplayName, c.total, c.orderID
              FROM `order_payment` a, tbl_suborder b, ordertb c, payment_type_master d
              WHERE a.`orderID` = b.orderID
              and a.orderID = c.orderId
              and a.paymentType = d.typeID;
              SELECT ifnull(sum(a.amount), 0) as paid, GROUP_CONCAT(DISTINCT(b.`orderNumber`)) as orderNumbers, c.total, c.orderID
              FROM ordertb c
              JOIN tbl_suborder b
              ON b.orderID = c.orderId
              LEFT JOIN `order_payment` a
              ON a.orderID = c.orderId
              and a.status = 1
              GROUP BY c.`orderId`
              order by b.status asc;
            ";
      return $this->bml_database->getResults($sql);
    }

    public function getOrderListByStatus($status,$locationKey)
    {
     if($locationKey=='MYSR'){
        $sql = "select t.total, t.subOrderID, t.orderId, t.`orderNumber`, t.customerName, t.mobileNumber, t.orderTotal, t.collectDate, t.startDate, t.endDate, t.returnDate,t.itemName, ifnull(t3.paid, 0) as paid
    			from 
				(
				  select t1.total, t1.subOrderID, t1.orderId, t1.`orderNumber`, t1.customerName, t1.mobileNumber, t1.orderTotal, t1.collectDate, t1.startDate, t1.endDate, t1.returnDate,t2.itemName 
				  from (
					select b.total, a.`subOrderID`, b.orderId, a.`orderNumber`, c.customerNumber, concat(c.firstName, ' ', c.lastName) as customerName, c.mobileNumber, ROUND((a.`orderSubtotal`-a.`orderDiscountAmount`),2) as orderTotal, e.collectDate, e.startDate, e.endDate, e.returnDate, d.orderStatusDisplayName
					FROM `tbl_suborder` a
					JOIN ordertb b
					on a.orderID = b.orderId
					JOIN customer c
					on b.customerId = c.customerId
					JOIN tbl_order_status d
					on a.status = d.orderStatusID
					JOIN orderproduct e
					on a.subOrderID = e.subOrderID
					where d.orderStatsCode = '$status' and e.locationKey='$locationKey'
					group by a.subOrderID
				  ) t1,
				  (
					SELECT REPLACE(GROUP_CONCAT(a.`itemName`),',','<br>') as itemName, c.`subOrderID`
					FROM `itemmaster` a
					JOIN orderproduct b
					ON a.`itemId` = b.`itemId`
					JOIN tbl_suborder c
					ON b.`subOrderID` = c.`subOrderID`
					JOIN tbl_order_status d
					ON c.`status` = d.`orderStatusID`
					where d.`orderStatsCode` = '$status'
					GROUP BY c.`subOrderID`
				  ) t2
				  where t1.subOrderID = t2.subOrderID
				) t
				left join
				(
				select sum(f.amount) as paid, f.orderID
				from order_payment f
				where f.status = 1
				group by f.orderID
				) t3
        on t.orderId = t3.orderID";
        }
        else{
          $sql = "select t.total, t.subOrderID, t.orderId, t.`orderNumber`, t.customerName, t.mobileNumber, t.orderTotal, t.collectDate, t.startDate, t.endDate, t.returnDate,t.itemName, ifnull(t3.paid, 0) as paid
          from 
          (
            select t1.total, t1.subOrderID, t1.orderId, t1.`orderNumber`, t1.customerName, t1.mobileNumber, t1.orderTotal, t1.collectDate, t1.startDate, t1.endDate, t1.returnDate,t2.itemName 
            from (
            select b.total, a.`subOrderID`, b.orderId, a.`orderNumber`, c.customerNumber, concat(c.firstName, ' ', c.lastName) as customerName, c.mobileNumber, ROUND((a.`orderSubtotal`-a.`orderDiscountAmount`),2) as orderTotal, e.collectDate, e.startDate, e.endDate, e.returnDate, d.orderStatusDisplayName
            FROM `tbl_suborder` a
            JOIN ordertb b
            on a.orderID = b.orderId
            JOIN customer c
            on b.customerId = c.customerId
            JOIN tbl_order_status d
            on a.status = d.orderStatusID
            JOIN orderproduct e
            on a.subOrderID = e.subOrderID
            where d.orderStatsCode = '$status'
            group by a.subOrderID
            ) t1,
            (
            SELECT REPLACE(GROUP_CONCAT(a.`itemName`),',','<br>') as itemName, c.`subOrderID`
            FROM `itemmaster` a
            JOIN orderproduct b
            ON a.`itemId` = b.`itemId`
            JOIN tbl_suborder c
            ON b.`subOrderID` = c.`subOrderID`
            JOIN tbl_order_status d
            ON c.`status` = d.`orderStatusID`
            where d.`orderStatsCode` = '$status'
            GROUP BY c.`subOrderID`
            ) t2
            where t1.subOrderID = t2.subOrderID
          ) t
          left join
          (
          select sum(f.amount) as paid, f.orderID
          from order_payment f
          where f.status = 1
          group by f.orderID
          ) t3
          on t.orderId = t3.orderID";

        }
				//echo $sql;die();
      return $this->bml_database->getResults($sql);
    }

    public function changeOrderProductPrice($orderProductID, $price, $rentalPrice){
      $sql ="call changeOrderProductPrice('$orderProductID', '$price', '$rentalPrice')";
      return $this->bml_database->getResults($sql);
    }
    public function getItemsBySuborderID($id)
    {
      $sql = "SELECT  a.`orderProductId`,
                      a.`qty`,
                      a.`collectDate`,
                      a.`startDate`,
                      a.`endDate`,
					  a.`itemId`,
                      a.`returnDate`,
                      a.`duration`,
                      a.`rentalPrice`,
                      b.itemName,
                      c.orderStatusDisplayName
              FROM `orderproduct` a
              JOIN itemmaster b
              on a.`itemId` = b.itemId
              JOIN tbl_order_status c
              on a.status = c.orderStatusID
              WHERE a.`subOrderID` = $id";
			  
      return $this->bml_database->getResults($sql);
    }

    public function getOrderAddress($orderID){
      $sql = "SELECT 'delievery' as shippingType, `addressLine1`, `addressLine2`, `addressCity`, `addressState`, `addressPin`, `addressLandmark`, `addressType`, `shippingCost` FROM `customer_address` a, ordertb b
              WHERE a.`addressID` =  b.deliveryAddress and b.orderId = $orderID;
              SELECT 'pickup' as shippingType, `addressLine1`, `addressLine2`, `addressCity`, `addressState`, `addressPin`, `addressLandmark`, `addressType`, `shippingCost` FROM `customer_address` a, ordertb b
              WHERE a.`addressID` =  b.pickupAddress and b.orderId = $orderID;";
      return $this->bml_database->getResults($sql);
    }

    public function getOrderDetailsBySuborderID($suborderID)
    {
      $sql = "select c.comment as cust_comments, a.rating, a.`subOrderID`, a.status, b.orderId, a.`orderNumber`, c.customerNumber, concat(c.firstName, ' ', c.lastName) as customerName, a.`orderSubtotal`, a.`orderDiscountAmount`, (a.`orderSubtotal`-a.`orderDiscountAmount`) as orderTotal, e.startDate, e.endDate, d.orderStatusDisplayName, a.comments
              FROM `tbl_suborder` a
              JOIN ordertb b
              on a.orderID = b.orderId
              JOIN customer c
              on b.customerId = c.customerId
              JOIN tbl_order_status d
              on a.status = d.orderStatusID
              JOIN orderproduct e
              on a.subOrderID = e.subOrderID
              where a.`subOrderID` = $suborderID";
      $result = $this->bml_database->getResults($sql);
		
      $returnArr = array();
      if(array_key_exists(0, $result)) //resultset1
      {
        $result = $result[0];
        if(array_key_exists(0, $result)) //row1
        {
          $returnArr[0] = $result;
          $orderID = $result[0]['orderId'];

          $result = $this->getOrderAddress($orderID);
          foreach ($result as $resultSet) {
            if(array_key_exists(0, $result))
            {
              $returnArr[1][] = $resultSet[0];
            }
          }

          $result = $this->getItemsBySuborderID($suborderID);
          if(array_key_exists(0, $result)) //resultset1
          {
            $returnArr[2] = $result[0];
          }
          else
          {
            $returnArr[2] = array();
          }
        }
      }
      return $returnArr;
    }

    public function getClientDetailsByClientNumber($clientNumber){
      $sql = "SELECT `customerId` INTO @id  FROM `customer` WHERE `customerNumber` = '$clientNumber';
              SELECT * FROM `customer` WHERE `customerId` = @id;
              select * from customer_address WHERE `customerId` = @id;";
              
      return $this->bml_database->getResults($sql);
    }
    public function saveAddressStatus($addressID, $status, $shippingcost = '`shippingCost`')
    {
      $sql = "UPDATE `customer_address` SET `shippingCost`='$shippingcost',`status`=$status WHERE `addressID` = $addressID";
      return $this->bml_database->getResults($sql);
    }
    public function saveCustomerStatus($customerNumber, $status, $comments = '')
    {
      $sql = "UPDATE `customer` SET `comments`='$comments', `status` = '$status' where `customerNumber` = '$customerNumber'";
      return $this->bml_database->getResults($sql);
    }
    public function getProductDetails()
    {
      $sql = "SELECT a.itemId,a.`itemName`,b.`itemTypeName`,d.`category_Name`,c.`itemBrandName`,  a.`numStock`,a.`itemStatus`
              FROM `itemmaster` a
              JOIN tbl_item_type b
              on a.`itemType` = b.itemTypeID
              JOIN tbl_item_brand c
              on a.`itemBrand` = c.itemBrandID
              JOIN category_tb d
              on a.`itemCategory` = d.categoryID";

      return $this->bml_database->getResults($sql);
    }
    public function getProductDetailsByStatusActive()
    {
      $sql = "SELECT a.itemId,a.`itemName`,b.`itemTypeName`,d.`category_Name`,c.`itemBrandName`,  a.`numStock`,a.`itemStatus`
              FROM `itemmaster` a
              JOIN tbl_item_type b
              on a.`itemType` = b.itemTypeID
              JOIN tbl_item_brand c
              on a.`itemBrand` = c.itemBrandID
              JOIN category_tb d
              on a.`itemCategory` = d.categoryID
              WHERE a.`itemStatus`='1' ";

      return $this->bml_database->getResults($sql);
    }
    public function getDiscountDetails()
    {
      $sql = "SELECT * FROM `discount`";
      return $this->bml_database->getResults($sql);
    }
    public function viewDiscountDetailsByID($id)
    {
      $sql = "SELECT * FROM `discount` WHERE `discountId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    public function getCustomer()
    {
      $sql = "SELECT * FROM `customer`";
      return $this->bml_database->getResults($sql);
    }
    public function viewProductDetails($id)
    {
      $sql = "SELECT a.itemId,a.`itemName`,b.`itemTypeName`,d.`category_Name`,c.`itemBrandName`,  a.`numStock`,a.`itemStatus`, a.`itemDescription`, a.`itemImage1`, a.`itemImage2`, a.`itemSampleImagePath`,  a.`itemSpecification`, e.`Stock` , e.`locationKey`
              FROM `itemmaster` a
              JOIN tbl_item_type b
              on a.`itemType` = b.itemTypeID
              JOIN tbl_item_brand c
              on a.`itemBrand` = c.itemBrandID
              JOIN category_tb d
              on a.`itemCategory` = d.categoryID
              JOIN location_qty e
              ON e.itemId='".$id."'
              WHERE a.itemId='".$id."'";
     
      return $this->bml_database->getResults($sql);
    
    }
    public function getItemType()
    {
      $sql="SELECT * FROM `tbl_item_type`";
      return $this->bml_database->getResults($sql);
    }
    
    public function getBrandName()
    {
      $sql="SELECT * FROM `tbl_item_brand`";
      return $this->bml_database->getResults($sql);
    }
    public function getCategoryName()
    {
      $sql="SELECT * FROM `category_tb`";
      return $this->bml_database->getResults($sql);
    }
    public function getPriceByID($id)
    {
      $sql="SELECT * FROM `pricemaster` WHERE `itemId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }

    public function updateProductDetails($itemId,$itemSpecifi,$itemname,$itemType,$category,$itemBrand,$itemstatus,$itemDescription)
    {
     $sql="UPDATE `itemmaster` SET `itemType`=".$itemType.",`itemBrand`=".$itemBrand.",`itemCategory`=".$category.",`itemDescription`=".($itemDescription).",`itemName`=".$itemname.",`itemStatus`=".$itemstatus.",`itemSpecification`=".$itemSpecifi." WHERE `itemId`=".$itemId;
     //echo $sql; die();
	 return $this->bml_database->getResults($sql);
    }
    public function updateProductQtyBlrDetails($itemId,$blrstocks,$keyblr)
    {
 
     $sql="UPDATE `location_qty` SET `stock`=".$blrstocks." WHERE `itemId`=".$itemId." AND locationKey=$keyblr";

     //echo $sql; die();
  	 return $this->bml_database->getResults($sql);
    }
    public function updateProductQtyMysrDetails($itemId,$mysrstocks,$keymysr)
    {
 
     $sql="UPDATE `location_qty` SET `stock`=".$mysrstocks." WHERE `itemId`=".$itemId." AND locationKey=$keymysr";

     //echo $sql; die();
  	 return $this->bml_database->getResults($sql);
    }

    public function deleteProductByID($id)
    {
      $sql="DELETE FROM `itemmaster` WHERE `itemId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    public function insertItemType($itemTypeName)
    {
      $sql="INSERT INTO `tbl_item_type`(`itemTypeName`) VALUES ('".$itemTypeName."')";
      return $this->bml_database->getResults($sql);
    } 
    public function insertBrandType($itemBrandName)
    {
      $sql="INSERT INTO `tbl_item_brand`(`itemBrandName`) VALUES ('".$itemBrandName."')";
      return $this->bml_database->getResults($sql);
    }
    public function insertCategoryType($category_Name,$displayOrder)
    {
      $sql="INSERT INTO `category_tb` (`category_Name`, `displayOrder`) VALUES ('".$category_Name."','".$displayOrder."')";
      return $this->bml_database->getResults($sql);
    }
    public function insertProductDetails($itemType,$itemBrand,$itemCategory,$itemDescription,$itemImage1,$itemImage2,$itemName,$itemStatus,$itemSpecification)
    {
      $sql="INSERT INTO `itemmaster`(`itemType`, `itemBrand`, `itemCategory`, `itemDescription`, `itemImage1`, `itemImage2`, `itemName`, `itemStatus`, `itemSpecification`) VALUES (".$itemType.",".$itemBrand.",".$itemCategory.",".$itemDescription.",'".$itemImage1."','".$itemImage2."',".$itemName.",".$itemStatus.",".$itemSpecification.")";
     
      return $this->bml_database->getResults($sql);
     
    }

    public function insertBlrqtyDetails($itemStockBlr,$itemBlrKey,$itemMysrKey,$itemId)
    {
     
     $sql1= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (".$itemStockBlr.",".$itemId.",".$itemBlrKey.")";
    $sql2= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (0,".$itemId.",".$itemMysrKey.")";
     $this->bml_database->getResults($sql1); 
  $this->bml_database->getResults($sql2);
    }

    public function insertMysrqtyDetails($itemStockMysr,$itemBlrKey,$itemMysrKey,$itemId)
    {
     
      $sql1= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (0,".$itemId.",".$itemBlrKey.")";
      $sql2= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (".$itemStockMysr.",".$itemId.",".$itemMysrKey.")";

      $this->bml_database->getResults($sql1); 
      $this->bml_database->getResults($sql2); 

    }
    public function insertLocationqtyDetails($itemStockBlr,$itemStockMysr,$itemBlrKey,$itemMysrKey,$itemId)
    {
        $sql2= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (".$itemStockMysr.",".$itemId.",".$itemMysrKey.")";
        $sql1= "INSERT INTO `location_qty`(`stock`, `itemId` , `locationKey`)VALUES (".$itemStockBlr.",".$itemId.",".$itemBlrKey.")";
        
   
        $this->bml_database->getResults($sql1); 
        $this->bml_database->getResults($sql2); 

    }

  

    public function insertHomeSlideDetails($slidetitle,$slidecontent,$slideBGImage,$slideItemImage,$slideItemID)
    {
      $sql="INSERT INTO `homeslidercontent`(`popupTitle`, `popupDesc`, `popupItemImage`, `popupImage`, `itemId`) VALUES ('".$slidetitle."','".$slidecontent."','".$slideBGImage."','".$slideItemImage."','".$slideItemID."')";
      return $this->bml_database->getResults($sql);
    }
    public function updateItemImage($itemImage1,$itemImage2,$itemId)
    {
	  if($itemImage1 != null)
		$sql="UPDATE `itemmaster` SET `itemImage1`='".$itemImage1."' WHERE `itemId`='".$itemId."'";
	  else if($itemImage2 != null)
		$sql="UPDATE `itemmaster` SET `itemImage2`='".$itemImage2."' WHERE `itemId`='".$itemId."'";
      return $this->bml_database->getResults($sql);
    }
    public function updateDiscountDetails($Id,$discount,$discountCount,$promocode,$customerIds,$status)
    {
      $sql="UPDATE `discount` SET `discount`='".$discount."',`discountCount`='".$discountCount."',`promocode`='".$promocode."',`customerIds`='".$customerIds."',`status`='".$status."' WHERE `discountId`='".$Id."'";
      return $this->bml_database->getResults($sql);
    }
    public function insertDiscountDetails($discount,$discountCount,$promocode,$customerIds,$status)
    {
      echo $sql="INSERT INTO `discount`( `discount`, `discountCount`, `promocode`, `customerIds`, `status`)
       VALUES('".$discount."','".$discountCount."','".$promocode."','".$customerIds."','".$status."')";
      return $this->bml_database->getResults($sql);
    }
    public function getLatestDetails()
    {
      $sql="SELECT * FROM `tbl_latest`";
      return $this->bml_database->getResults($sql);
    }
    public function getFeaturedDetails()
    {
      $sql="SELECT * FROM `featured_equipments`";
      return $this->bml_database->getResults($sql);
    }
    public function getHomeSliderDetails()
    {
      $sql="SELECT * FROM `homeslidercontent`";
      return $this->bml_database->getResults($sql);
    }
    public function removeLatest($id)
    {
      $sql="DELETE FROM `tbl_latest` WHERE `latest_id`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    public function insertLatestItem($itemID)
    {
      $sql="INSERT INTO `tbl_latest`(`itemId`) VALUES ('".$itemID."')";
      return $this->bml_database->getResults($sql);
    }
    public function removeFeatured($id)
    {
      $sql="DELETE FROM `featured_equipments` WHERE `featuredequipId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    public function insertFeaturedItem($itemID)
    {
      $sql="INSERT INTO `featured_equipments`(`itemId`) VALUES ('".$itemID."')";
      return $this->bml_database->getResults($sql);
    }
    public function getHomeSliderDetailsById($id)
    {
      $sql="SELECT * FROM `homeslidercontent` WHERE `popupId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    public function updateHomeSlideBGImage($slideBGImage,$itemId)
    {
      $sql="UPDATE `homeslidercontent` SET `popupItemImage`='".$slideBGImage."' WHERE `popupId`='".$itemId."'";
      return $this->bml_database->getResults($sql);

    }
    public function updateHomeSlideItemImage($slideItemImage,$itemId)
    {
      $sql="UPDATE `homeslidercontent` SET `popupImage`='".$slideItemImage."' WHERE `popupId`='".$itemId."'";
      return $this->bml_database->getResults($sql);
    }
    public function removeHomeSlider($id)
    {
      $sql="DELETE FROM `homeslidercontent` WHERE `popupId`='".$id."'";
      return $this->bml_database->getResults($sql);
    }
    //to get customer vocher points
    public function getDiscountPoints($customerId){
      $sql="SELECT a.`discount` 
            FROM  `discount` a
            JOIN  `historysuborder` b ON b.orderDiscountID = a.`discountId` 
            WHERE a.customerIds ='".$customerId."'";
      return $this->bml_database->getResults($sql);

    }
    // updating dicount table
    public function updateDiscountPoints($customerId,$discount){
      $sql="UPDATE `points_vallet` SET `points`=SUM(`EARNED_POINTS`) - SUM( `SPENT_POINTS` ) WHERE  `customerId`='".$customerId."'";
      return $this->bml_database->getResults($sql);
    }
}
