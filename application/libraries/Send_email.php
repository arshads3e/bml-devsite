<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');

 class Send_email {



  private $CI;

  private $fromEmail;



  public function __construct()

  {

    //$this->fromEmail = "rentals@bookmylens.com";
    $this->fromEmail = "arshads@3esofttech.com";

    //$this->bccEmail = "rentals@bookmylens.com";
    $this->bccEmail = "arshads@3esofttech.com";

    

	$this->CI =& get_instance();

    $this->CI->load->library('email');

    $config['mailtype'] = 'html';

    $this->CI->email->initialize($config);

  }



  public function sendWelcomeEmail($sendMail)

  {

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $data['code']=$sendMail->code;

      $data['locationKey'] =$sendMail->locationKey;

     

      

	  $subject="Signup | ".$sendMail->customerNumber;



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/welcomeEmail',$data,true);

       if($data['locationKey']=="BNGLR"){

          // $this->bccEmail="rentals@bookmylens.com";
          $this->bccEmail="arshads@3esofttech.com";

       } else {

           //$this->bccEmail="rentals_mysore@bookmylens.com";
           $this->bccEmail="arshads@3esofttech.com";

       }

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail="arshads@3esofttech.com";
      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->bcc($this->bccEmail);

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->to($list);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);

      return $this->CI->email->send();

  }

  

   public function sendPassResetEmail($sendMail)

  {

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $data['password']=$sendMail->pass;

      

	  $subject="Password Reset Complete | ".$sendMail->customerNumber;



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/forgotPass',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->bcc($this->bccEmail);

      $this->CI->email->to($list);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);

       //$this->ccEmail= "rentals_mysore@bookmylens.com";
       $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      return $this->CI->email->send();

  }

  

  public function sendRegistrationSuccessEmail($sendMail)

  {

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $subject= "Update on Registration Details";



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/registrationSuccessEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->bcc($this->bccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);

 

      return $this->CI->email->send();

  }

  

  public function sendApprovalSuccessEmail($sendMail)

  {

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $subject= "Greetings | ".$sendMail->customerNumber;



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/approvalSuccessEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

  }

  
  public function sendOrderFailureSummaryEmail($sendMail,$paymentType){	

        $sendMail=(object)$sendMail;
  
        $data['email']=$sendMail->email;  
        $to=$sendMail->email;  
        $data['firstName']=$sendMail->firstName;  
        $data['subOrderItems']=$sendMail->subOrderItems;  
        $data['total']=$sendMail->total;  
        $txnid=$data['txnFailed']=$sendMail->txnFailed;
        //$data['shippingCost']=$sendMail->shippingCost; 
         
        $subject="$paymentType Payment Failed | ".$data['firstName']."(".$data['customerNumber'].") | ".$txnid;

        $email_body = "$paymentType Payment Failed for Transaction ID is $txnid. Please contact customer as soon as possible";
        
        $this->CI->email->set_newline("\r\n");
  
        //$email_body = $this->CI->load->view('emails/orderSummaryEmail',$data,true);
  
        $this->CI->email->from($this->fromEmail, 'BookMyLens');
  
        $list = array($to);
  
        $this->CI->email->to($list);
  
        $this->CI->email->bcc($this->bccEmail);
  
        //$this->ccEmail= "rentals_mysore@bookmylens.com";
        $this->ccEmail= "arshads@3esofttech.com";
  
        $this->CI->email->cc($this->ccEmail);
  
        $this->CI->email->subject($subject);
  
        $this->CI->email->message($email_body);
  
  
  
        return $this->CI->email->send();
  
    
  }
  public function sendOrderSummaryEmail($sendMail)

  {

		

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $data['subOrderItems']=$sendMail->subOrderItems;

      $data['total']=$sendMail->total;

	  $data['shippingCost']=$sendMail->shippingCost;

	  if(isset($sendMail->points_vallet_summary))

	  {

		$data['pointsEarned'] = isset($sendMail->points_vallet_summary['EARNED']) ? $sendMail->points_vallet_summary['EARNED'] : 0;

		$data['pointsSpent'] = isset($sendMail->points_vallet_summary['SPENT']) ? $sendMail->points_vallet_summary['SPENT'] : 0;

	  }

	  if(isset($sendMail->pumAmt))

		$data['pumAmt'] = $sendMail->pumAmt;

		

      $subject= "Order Summary | ".$sendMail->customerNumber;



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/orderSummaryEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

  }

  

  public function sendPaymentOrderSummaryEmail($sendMail)

  {

      $sendMail=(object)$sendMail;

      $data['email']=$sendMail->email;

      $to=$sendMail->email;

      $data['firstName']=$sendMail->firstName;

      $data['subOrderItems']=$sendMail->subOrderItems;

      $data['total']=$sendMail->total;

	  if(isset($sendMail->pumAmt))

		$data['pumAmt'] = $sendMail->pumAmt;

		

      $subject= "Payment Order Summary | ".$sendMail->customerNumber;



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/orderPaymentSummaryEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

  }



  public function sendOrderApprovalEmail($sendMail, $suborderID)

  {

      $sql = "SELECT `customerNumber`, `firstName`, `emailId` FROM `customer` WHERE `customerId` = ( SELECT `customerId` FROM `ordertb` WHERE `orderId` = ( SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '".$suborderID."'))";

		

		$res = $this->CI->bml_database->getResults($sql);

      $res = $res[0][0];

	  $data['email']=$res['emailId'];

      $to=$res['emailId'];

      $data['firstName']=$res['firstName'];

	  $sendMail = (object)$sendMail;

      $data['subOrderItems']=$sendMail->subOrderItems;

      $data['total']=$sendMail->total;

      $data['shipCost']=$sendMail->shipCost;

	  $subject= "Order Approved | ".$res['customerNumber'];



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/orderApprovalEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

	  

  }



  public function sendOrderCompleteEmail($sendMail, $suborderID)

  {

      

      $sql = "SELECT `customerNumber`, `firstName`, `emailId` FROM `customer` WHERE `customerId` = ( SELECT `customerId` FROM `ordertb` WHERE `orderId` = ( SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '".$suborderID."'))";

		

		$res = $this->CI->bml_database->getResults($sql);

      $res = $res[0][0];

	  $data['email']=$res['emailId'];

      $to=$res['emailId'];

      $data['firstName']=$res['firstName'];

	  $sendMail = (object)$sendMail;

	  $data['subOrderItems']=$sendMail->subOrderItems;

      

	  $subject= $res['customerNumber']." | Completed";



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/orderReturnEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

  }



  public function sendOrderCancelEmail($sendMail, $suborderID)

  {

      $sql = "SELECT `customerNumber`, `firstName`, `emailId` FROM `customer` WHERE `customerId` = ( SELECT `customerId` FROM `ordertb` WHERE `orderId` = ( SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '".$suborderID."'))";

		

		$res = $this->CI->bml_database->getResults($sql);

      $res = $res[0][0];

	  $data['email']=$res['emailId'];

      $to=$res['emailId'];

      $data['firstName']=$res['firstName'];

	  $sendMail = (object)$sendMail;

	  $data['subOrderItems']=$sendMail->subOrderItems;

	  $subject= "Cancellation | ".$res['customerNumber'];



      $this->CI->email->set_newline("\r\n");

      $email_body = $this->CI->load->view('emails/orderCancelEmail',$data,true);

      $this->CI->email->from($this->fromEmail, 'BookMyLens');

      $list = array($to);

      $this->CI->email->to($list);

      $this->CI->email->bcc($this->bccEmail);

      //$this->ccEmail= "rentals_mysore@bookmylens.com";
      $this->ccEmail= "arshads@3esofttech.com";

      $this->CI->email->cc($this->ccEmail);

      $this->CI->email->subject($subject);

      $this->CI->email->message($email_body);



      return $this->CI->email->send();

  }



  

  

}

