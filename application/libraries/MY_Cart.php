<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Cart extends CI_Cart {

  protected $pickupAddrID;
  protected $returnAddrID;
  public $errorMsg;

  public function __construct($params = array())
  {
    parent::__construct($params);
	  $this->errorMsg = '';
    $this->product_name_safe = false;

    if(count($this->_cart_contents) <= 2)
    {
      $this->_cart_contents['discount_value'] = 0;
	    $this->_cart_contents['discount_type'] = 0;
      $this->_cart_contents['discount_code'] = "";
      $this->_cart_contents['discountID'] = -1;
      $this->_cart_contents['cart_discount'] = 0;
      $this->_cart_contents['cart_subtotal'] = 0;
    }
  }

  public function getDiscountCode()
  {
    return $this->_cart_contents['discount_code'];
  }
  private function setDiscountCode($discountCode)
  {
      $this->_cart_contents['discount_code'] = $discountCode;
  }
  
  public function getDiscountType()
  {
    return $this->_cart_contents['discount_type'];
  }
  private function setDiscountType($discountType)
  {
      $this->_cart_contents['discount_type'] = $discountType;
  }

  public function getDiscount()
  {
    return $this->_cart_contents['discount_value'];
  }
  private function setDiscount($discount)
  {
      $this->_cart_contents['discount_value'] = $discount;
  }

  public function getDiscountID()
  {
    return $this->_cart_contents['discountID'];
  }
  private function setDiscountID($discountID)
  {
      $this->_cart_contents['discountID'] = $discountID;
  }

  public function getPickupAddrID()
  {
    return $this->pickupAddrID;
  }
  public function setPickupAddrID($pickupAddrID)
  {
    $this->pickupAddrID = $pickupAddrID;
  }

  public function getReturnAddrID()
  {
    return $this->returnAddrID;
  }
  public function setReturnAddrID($returnAddrID)
  {
    $this->returnAddrID = $returnAddrID;
  }

  public function applyDiscount($discountID = -1, $discountCode='', $discount=0, $discountType = 'PERCENT')
  {
    $this->setDiscount($discount);
    $this->setDiscountCode($discountCode);
    $this->setDiscountID($discountID);
	$this->setDiscountType($discountType);

    foreach ($this->_cart_contents as $key => $val)
    {
      // We make sure the array contains the proper indexes
      if ( ! is_array($val) OR ! isset($val['price'], $val['qty']))
      {
        continue;
      }
	  if($discountType == "FIXED")
	  {
		$this->_cart_contents[$key]['discountAmount'] = 0;
	  }
	  else {
		  $totalPrice = $this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty'];
		  $totalPriceBeforeTax = $totalPrice / 1.145; // 14.5% VAT
		  $this->_cart_contents[$key]['discountAmount'] = ($totalPriceBeforeTax) * $discount / 100;
	  }
    }
    $this->_save_cart();
  }

  public function clearDiscount()
  {
    $this->applyDiscount();//without parameters will clear cart
  }

  public function _insert($items = array())
  {
    // Was any cart data passed? No? Bah...
    if ( ! is_array($items) OR count($items) === 0)
    {
		$this->errorMsg = 'The insert method must be passed an array containing data.';
      log_message('error', 'The insert method must be passed an array containing data.');
      return FALSE;
    }

    // --------------------------------------------------------------------

    // Does the $items array contain an id, quantity, price, and name?  These are required
    if ( ! isset($items['id'], $items['qty'], $items['price'], $items['name']))
    {
		$this->errorMsg = 'The cart array must contain a product ID, quantity, price, and name.';
      log_message('error', 'The cart array must contain a product ID, quantity, price, and name.');
      return FALSE;
    }

    // --------------------------------------------------------------------

    // Prep the quantity. It can only be a number.  Duh... also trim any leading zeros
    $items['qty'] = (float) $items['qty'];

    // If the quantity is zero or blank there's nothing for us to do
    if ($items['qty'] == 0)
    {
      return FALSE;
    }

    // --------------------------------------------------------------------

    // Validate the product ID. It can only be alpha-numeric, dashes, underscores or periods
    // Not totally sure we should impose this rule, but it seems prudent to standardize IDs.
    // Note: These can be user-specified by setting the $this->product_id_rules variable.
    if ( ! preg_match('/^['.$this->product_id_rules.']+$/i', $items['id']))
    {
		$errorMsg = 'Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores';
      log_message('error', 'Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores');
      return FALSE;
    }

    // --------------------------------------------------------------------

    // Validate the product name. It can only be alpha-numeric, dashes, underscores, colons or periods.
    // Note: These can be user-specified by setting the $this->product_name_rules variable.
    if ($this->product_name_safe && ! preg_match('/^['.$this->product_name_rules.']+$/i'.(UTF8_ENABLED ? 'u' : ''), $items['name']))
    {
		$errorMsg = 'An invalid name was submitted as the product name: '.$items['name'].' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces';
      log_message('error', 'An invalid name was submitted as the product name: '.$items['name'].' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces');
      return FALSE;
    }

    // --------------------------------------------------------------------

    // Prep the price. Remove leading zeros and anything that isn't a number or decimal point.
    $items['price'] = (float) $items['price'];

    //calculate discount
    if($this->getDiscount() > 0)
    {
	  if($this->getDiscountType() == "FIXED")
	  {
		$items['discountAmount'] = 0;
	  }
	  else 
	  {
		$items['discountAmount'] = (($items['qty'] * $items['price']) / 1.145) * $this->getDiscount() / 100;
      }
	}
    else {
      $items['discountAmount'] = 0;
    }

    // We now need to create a unique identifier for the item being inserted into the cart.
    // Every time something is added to the cart it is stored in the master cart array.
    // Each row in the cart array, however, must have a unique index that identifies not only
    // a particular product, but makes it possible to store identical products with different options.
    // For example, what if someone buys two identical t-shirts (same product ID), but in
    // different sizes?  The product ID (and other attributes, like the name) will be identical for
    // both sizes because it's the same shirt. The only difference will be the size.
    // Internally, we need to treat identical submissions, but with different options, as a unique product.
    // Our solution is to convert the options array to a string and MD5 it along with the product ID.
    // This becomes the unique "row ID"
    if (isset($items['options']) && count($items['options']) > 0)
    {
      $rowid = md5($items['id'].serialize($items['options']));
    }
    else
    {
      // No options were submitted so we simply MD5 the product ID.
      // Technically, we don't need to MD5 the ID in this case, but it makes
      // sense to standardize the format of array indexes for both conditions
      $rowid = md5($items['id']);
    }

    // --------------------------------------------------------------------

    // Now that we have our unique "row ID", we'll add our cart items to the master array
    // grab quantity if it's already there and add it on
    $old_quantity = isset($this->_cart_contents[$rowid]['qty']) ? (int) $this->_cart_contents[$rowid]['qty'] : 0;

    // Re-create the entry, just to make sure our index contains only the data from this submission
    $items['rowid'] = $rowid;
    $items['qty'] += $old_quantity;
    $this->_cart_contents[$rowid] = $items;

    return $rowid;
  }

  public function subtotal()
  {
    return $this->_cart_contents['cart_subtotal'];
  }

  public function cartDiscount()
  {
    return $this->_cart_contents['cart_discount'];
  }

  public function _update($items = array())
  {
    // Without these array indexes there is nothing we can do
    if ( ! isset($items['rowid'], $this->_cart_contents[$items['rowid']]))
    {
      return FALSE;
    }

    // Prep the quantity
    if (isset($items['qty']))
    {
      $items['qty'] = (float) $items['qty'];
      // Is the quantity zero?  If so we will remove the item from the cart.
      // If the quantity is greater than zero we are updating
      if ($items['qty'] == 0)
      {
        unset($this->_cart_contents[$items['rowid']]);
        return TRUE;
      }
    }

    // find updatable keys
    $keys = array_intersect(array_keys($this->_cart_contents[$items['rowid']]), array_keys($items));
    // if a price was passed, make sure it contains valid data
    if (isset($items['price']))
    {
      $items['price'] = (float) $items['price'];
    }

    // product id & name shouldn't be changed
    foreach (array_diff($keys, array('id', 'name')) as $key)
    {
      $this->_cart_contents[$items['rowid']][$key] = $items[$key];
    }

    //calculate discount
    $this->_cart_contents[$items['rowid']]['discountAmount'] = ($this->_cart_contents[$items['rowid']]['qty'] * $this->_cart_contents[$items['rowid']]['price']) * $this->getDiscount() / 100;

    return TRUE;
  }

  public function _save_cart()
  {
    // Let's add up the individual prices and set the cart sub-total
    $this->_cart_contents['total_items'] = $this->_cart_contents['cart_total'] = $this->_cart_contents['cart_discount'] = $this->_cart_contents['cart_subtotal'] = 0;
    foreach ($this->_cart_contents as $key => $val)
    {
      // We make sure the array contains the proper indexes
      if ( ! is_array($val) OR ! isset($val['price'], $val['qty']))
      {
        continue;
      }
      //if discountAmount key not present set 0
      if(!array_key_exists('discountAmount', $val)){
        $val['discountAmount'] = 0;
      }

      $this->_cart_contents[$key]['subtotal'] = ($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty']);

      if( ! $val['options']['waitingList']){
        $this->_cart_contents['cart_total'] += ($val['price'] * $val['qty'] - $val['discountAmount']);
        $this->_cart_contents['total_items'] += $val['qty'];
        $this->_cart_contents['cart_discount'] += $val['discountAmount'];
        $this->_cart_contents['cart_subtotal'] += $this->_cart_contents[$key]['subtotal'];
      }
    }
	
	if($this->getDiscountType() == 'FIXED')
	{
		$this->_cart_contents['cart_discount'] = $this->getDiscount();
		$this->_cart_contents['cart_total'] -= $this->_cart_contents['cart_discount'];
	}

    // Is our cart empty? If so we delete it from the session
    if (count($this->_cart_contents) <= 8)
    {
      $this->CI->session->unset_userdata('cart_contents');

      // Nothing more to do... coffee time!
      return FALSE;
    }

    // If we made it this far it means that our cart has data.
    // Let's pass it to the Session class so it can be stored
    $this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));

    // Woot!
    return TRUE;
  }

  public function contents($newest_first = FALSE)
  {
    // do we want the newest first?
    $cart = ($newest_first) ? array_reverse($this->_cart_contents) : $this->_cart_contents;

    // Remove these so they don't create a problem when showing the cart table
    unset($cart['total_items']);
    unset($cart['cart_total']);
    unset($cart['cart_discount']);
    unset($cart['cart_subtotal']);
    unset($cart['discount_value']);
    unset($cart['discount_type']);
    unset($cart['discountID']);
    unset($cart['discount_code']);

    return $cart;
  }

  public function placeOrder()
  {
    //start manual sql transaction
    $this->CI->db->trans_begin();
    //insert new order
    $sql = "INSERT INTO `ordertb`(`total`, `discountID`, `dicountAmount`, `status`, `customerId`, `deliveryAddress`, `pickupAddress`, `orderDate`)"
            ."VALUES ('".$this->total()."', ".$this->getDiscountID().", ".$this->cartDiscount().", 1, ".$this->CI->user_session->getSessionVar('customerId').", '".$this->getPickupAddrID()."', '".$this->getReturnAddrID()."', now())";
    $this->CI->bml_database->getResults($sql);
    $orderID = $this->CI->db->insert_id();

    //group products by start and end date
    $orders = [];
    foreach ($this->contents() as $items) {
      if(empty($orders))
      {
        $orders[] = array(
                      "start" => $items['options']['startDate'],
                      "end" => $items['options']['endDate'],
                      "waiting" => $items['options']['waitingList'],
                      "items" => array($items)
                    );
        continue;
      }
      $insert = false;
      for ($i=0; $i < count($orders); $i++) {
        if($orders[$i]['start'] == $items['options']['startDate']
            && $orders[$i]['end'] == $items['options']['endDate']
              && $orders[$i]['waiting'] == $items['options']['waitingList']){
          $orders[$i]['items'][] = $items;
          $insert = true;
        }
      }
      if(!$insert)
      {
        $orders[] = array(
                      "start" => $items['options']['startDate'],
                      "end" => $items['options']['endDate'],
                      "waiting" => $items['options']['waitingList'],
                      "items" => array($items)
                    );
      }
    }

    //insert each product group (suborder)
    foreach ($orders as $orderRow) {

      $sql = "INSERT INTO `tbl_suborder`(`orderID`, orderDiscountID) VALUES ($orderID, '".$this->getDiscountID()."')";
      $this->CI->bml_database->getResults($sql);
      $subOrderID = $this->CI->db->insert_id();
	    $orderNumber = "OBML".sprintf("%06d", $subOrderID);

      //insert each product of this suborder/subgroup
      $subTotal = 0; $discountAmount = 0;$orderStatus=1;
      foreach ($orderRow['items'] as $items) {
        $subTotal += $items['subtotal'];
        $discountAmount += $items['discountAmount'];
        $today =  new DateTime();
	    	$collectDate = date_create_from_format('d/M/Y', $items['options']['startDate']);
        $returnDate = date_create_from_format('d/M/Y', $items['options']['endDate']);
        $date1 = date_create_from_format('d/M/Y', $items['options']['startDate']);
        $date2 = date_create_from_format('d/M/Y', $items['options']['endDate']);
        $interval = $date1->diff($date2);
        $duration = $interval->days+1;

        $collectDate->sub(new DateInterval('P1D'));
        $returnDate->add(new DateInterval('P1D'));

        //check if there is any order one day before start date and one day after end date
		//check if it is holiday one day before start date and one day after end date
        $sql = "SELECT  count(1) as 'numOfOrders'
                FROM `orderproduct` a
                where a.`itemId` = '".$items['id']."'
                and a.`startDate` <= '".$collectDate->format('Y-m-d')."'
                and a.`endDate` >= '".$collectDate->format('Y-m-d')."' 
				AND  `status` IN ( 1, 2, 3, 8 );\n";
        $sql .= "SELECT  count(1) as 'numOfOrders'
                FROM `orderproduct` a
                where a.`itemId` = ".$items['id']."
                and a.`startDate` <= '".$returnDate->format('Y-m-d')."'
                and a.`endDate` >= '".$returnDate->format('Y-m-d')."'
				AND  `status` IN ( 1, 2, 3, 8 );\n";
    //echo $sql;
  
    $sql .= "SELECT `stock` FROM `location_qty` WHERE `itemId` = ".$items['id']." AND locationKey='MYSR';";
		$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$collectDate->format('Y-m-d')."';";
		$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$returnDate->format('Y-m-d')."';";
		$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$date1->format('Y-m-d')."';";
		$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$date2->format('Y-m-d')."';";
        $result = $this->CI->bml_database->getResults($sql);
        $totalQty = $result[2][0]['stock'];


		if(isset($result[3][0]['numRow']) && $result[3][0]['numRow'] == 1)
		{echo "426";
			$collectDate->add(new DateInterval('P1D'));
		}
		else if((isset($result[5][0]['numRow']) && $result[5][0]['numRow'] == 1)){}
        else if(($collectDate->format('N') != 6 && $result[0][0]['numOfOrders'] >= $totalQty) || $date1 == $today)
        {//echo $collectDate->format('N')." ".$result[0][0]['numOfOrders']." ".$totalQty;
          $collectDate->add(new DateInterval('P1D'));
        }
		else if($collectDate->format('N') == 7)
		{//echo "435";
          $collectDate->add(new DateInterval('P1D'));
		}
		//echo $collectDate->format('N');
		//$this->CI->db->trans_rollback();
		//die();
        if(isset($result[4][0]['numRow']) && $result[4][0]['numRow'] == 1)
		{
			$returnDate->sub(new DateInterval('P1D'));
		}
		else if((isset($result[6][0]['numRow']) && $result[6][0]['numRow'] == 1)){}
        else if($returnDate->format('N') == 7 || $result[1][0]['numOfOrders'] >= $totalQty)
        {
          $returnDate->sub(new DateInterval('P1D'));
        }
        $locationKey= $this->CI->user_session->getSessionVar('locationKey');
       
        $orderStatus = (($items['options']['waitingList'] == true)? '6' : '1') ;
        $sql = "INSERT INTO `orderproduct`(`startDate`, `endDate`, `collectDate`, `returnDate`, `subOrderID`, `itemId`, `duration`, `rentalTotalPrice`, `status`,`qty`, `locationKey`)
                VALUES ('".$date1->format('Y-m-d')."', '".$date2->format('Y-m-d')."', '".$collectDate->format('Y-m-d')."', '".$returnDate->format('Y-m-d')."', ".$subOrderID.", ".$items['id'].", ".$duration.", ".$items['subtotal'].", ".$orderStatus .", ".$items['qty'].", '".$locationKey."')";
         $this->CI->bml_database->getResults($sql);
       
      }

      //update suborderTotal
      $sql = "UPDATE `tbl_suborder` SET `orderNumber` = '".$orderNumber."', `orderSubtotal`=$subTotal, `orderDiscountAmount` = $discountAmount, `status` = $orderStatus WHERE `subOrderID` = $subOrderID";
      $this->CI->bml_database->getResults($sql);

    }
	
	//add shipping Details
	$sql = "insert into `shipping_tb` (`orderID`, `addressID`, `ship_type`, `ship_flg`) values ( '".$orderID."', '".$this->getPickupAddrID()."', 'P', 0);";
	$sql .= "insert into `shipping_tb` (`orderID`, `addressID`, `ship_type`, `ship_flg`) values ( '".$orderID."', '".$this->getReturnAddrID()."', 'R', 0);";
	$this->CI->bml_database->getResults($sql);
	
    //check transaction status
    if ($this->CI->db->trans_status() === FALSE)
    {
        $this->CI->db->trans_rollback();
        return array("status" => false, "message" => $this->CI->db->_error_message());
    }
    else
    {
        $this->CI->db->trans_commit();
        //$this->CI->db->trans_rollback();
        return array("status" => true, "orderID" => $orderID);
    }
  }

  public function get_item_by_id($prodectId)
  {
    $in_cart = array();

    if ($this->total_items() > 0)
    {
        // Fetch data for all products in cart
        foreach ($this->contents() AS $item)
        {
            if($item['id'] == $prodectId)
            {
              $in_cart[] = $item;
            }
        }
    }
    return $in_cart;
  }
}
