$body = $("body");
jQuery.fn.exists = function() { return jQuery(this).length > 0; }

function generateDiscount() {
    if (confirm("Are you sure ?") == true) {
        alert("Discount code has sent to you mobile");
        window.location.href = site_url + "myaccount/generateDiscount";
    }
}

$(document).ready(function() {
    disableAjaxLoadUI = false;
    $("#searchBox").on("keyup", function() {
        disableAjaxLoadUI = true;
    });
    $("#searchBox").autocomplete({
        source: site_url + "product/search"
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<a href = '" + item.link + "' style='font-size:18px;'><span " + item.spanStyle + ">" + item.label1 + "<span class='pull-right'>" + item.label2 + "</span></a>")
            .appendTo(ul);
    };
    $('.show-title').each(function() {
        $(this).hover(function() {
            $(this).parent().siblings(".product_view").fadeTo(0, .5);
            $(this).parent().siblings(".proddesc.makeVisible").fadeIn(50);

        }, function() {
            $(this).parent().siblings(".product_view").fadeTo(0, 1);
            $(this).parent().siblings(".proddesc.makeVisible").fadeOut(100);
        });
    });

    $('.show-price').each(function() {
        $(this).hover(function() {
            $(this).parent().siblings(".product_view").fadeTo(0, .5);
            $(this).parent().siblings(".price.makeVisible").fadeIn(50);
        }, function() {
            $(this).parent().siblings(".product_view").fadeTo(0, 1);
            $(this).parent().siblings(".price.makeVisible").fadeOut(100);
        });
    });

});

$(document).ready(function() {

    //menu
    function toggleAccordion(li) {
        if (li.hasClass('active')) {
            li.removeClass('active');
            $('.sub-menu', li).slideUp();
            $('i', li).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-down');
        } else {
            $('li.active .sub-menu').slideUp();
            $('li i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-down');
            $('li.active').removeClass('active');
            li.addClass('active');
            $('.sub-menu', li).slideDown();
            $('i', li).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-down');
        }
    };
    $('.sidebar ul li').click(function(ev) {
        ev.stopPropagation();
        toggleAccordion($(this));
    });

    $('.sidebar ul li a').click(function(ev) {
        ev.stopPropagation();
        toggleAccordion($(this).parent());
    });

    //rent page, select rental page
    if ($(".datepicker").exists()) {
        d = new Date();
        d1 = new Date();
        d1.setDate(d1.getDate() - 1);
        $(".datepicker").datepicker({
            minDate: 0,
            numberOfMonths: 2,
            dateFormat: 'dd/M/yy',
            todayHighlight: false,
            beforeShowDay: function(date) {
                //alert(date);
                if (date < d1)
                    return [false, ""];
                if (date.getFullYear() == d.getFullYear() &&
                    date.getMonth() == d.getMonth() &&
                    date.getDate() == d.getDate() &&
                    d.getHours() > 14)
                    return [false, ""];

                var date1 = $.datepicker.parseDate('dd/M/yy', $("#date1").val());
                var date2 = $.datepicker.parseDate('dd/M/yy', $("#date2").val());
                dateStr = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2);
                var cssClass = "";

                if (date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
                    cssClass = "dp-highlight";
                    jQuery.each(unAvailableDates, function(index, dateVal) {
                        var date = $.datepicker.parseDate("yy-mm-dd", dateVal);
                        if (date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
                            cssClass = "waiting";
                            //set waiting flag
                            waitingList = true;
                        }
                    });
                    return [true, cssClass];
                } else if (jQuery.inArray(dateStr, unAvailableDates) !== -1) {
                    return [true, "unavailable"];
                } else if (jQuery.inArray(dateStr, holidayList) !== -1) {
                    return [true, "closed"];
                }
                return [true, ""];
            },
            onSelect: function(dateText, inst) {
                //reset waiting flag;
                //it will be set in beforeShowDay() function, if product is not available for selected dates
                waitingList = false;
                var date1 = $.datepicker.parseDate('dd/M/yy', $("#date1").val());
                var date2 = $.datepicker.parseDate('dd/M/yy', $("#date2").val());
                var selectDate = $.datepicker.parseDate('dd/M/yy', dateText);
                if (!date1 || date2) {
                    $("#date1").val(dateText);
                    $("#date2").val("");

                    $("#ldate1").text(dateText);
                    $("#ldate2").text(dateText);

                    $(this).datepicker();
                    $('#days').val(1);
                    $('#ldays').text(1);
                    $('#price').val(priceArray[1]);
                    $('#lprice').text("INR " + priceArray[1]);

                } else if (selectDate >= date1) {
                    $("#date2").val(dateText);
                    $("#ldate2").text(dateText);
                    $(this).datepicker();
                    var days = ((selectDate - date1) / (1000 * 60 * 60 * 24)) + 1;
                    $('#days').val(days);
                    $('#ldays').text(days);
                    var price = priceArray[1];
                    for (i = days; i > 0; i--) {
                        if (priceArray[i] != undefined) {
                            price = priceArray[i];
                            break;
                        }
                    }
                    price *= days;
                    $('#price').val(price);
                    if (price == 0) {
                        $('#lprice').text("Contact Us");
                    } else {
                        $('#lprice').text("INR " + price);
                    }
                } else {
                    $("#date1").val("");
                    $("#date2").val("");
                    $("#ldate1").text("");
                    $("#ldate2").text("");
                    $('#days').val(0);
                    $('#ldays').text("");
                    $('#price').val("");
                    $('#lprice').text("");
                }

            }

        });
        addTitleTextRentCal();
    }
    //userRegister form ajax sumbit
    registerForm = $('#joinnowForm');
    registerForm.submit(function(ev) {
        ev.preventDefault();
        $.ajax({
            type: registerForm.attr('method'),
            url: registerForm.attr('action'),
            data: registerForm.serialize(),
            success: function(data) {
                var data = jQuery.parseJSON(data);
                if (data.status == "success") {
                    $('#signupModal').modal('hide');
                    alert(data.message);

                    window.location.href = smsVerification;
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    });

    //userLogin form ajax sumbit
    loginForm = $('#loginForm');
    loginForm.submit(function(ev) {
        ev.preventDefault();
        $.ajax({
            type: loginForm.attr('method'),
            url: loginForm.attr('action'),
            data: loginForm.serialize(),
            success: function(data) {
                var data = jQuery.parseJSON(data);
                if (data.status == "success") {
                    if (data.isAdmin == true) {
                        window.location.href = adminURL;
                    } else {
                        if (className == "product") {
                            window.location = window.location.href;
                        } else
                            window.location.href = myaccountURL;
                    }
                } else {
                    alert("invalid user/password");
                }
            }
        });
        return false;
    });

    //userLogin form ajax sumbit
    loginForm1 = $('#loginForm1');
    loginForm1.submit(function(ev) {
        ev.preventDefault();
        // console.log("form data", loginForm1.serialize());
        $.ajax({
            type: loginForm1.attr('method'),
            url: loginForm1.attr('action'),
            data: loginForm1.serialize(),



            success: function(data) {
                var data = jQuery.parseJSON(data);
                if (data.status == "success") {
                    if (data.isAdmin == true) {
                        window.location.href = adminURL;
                    } else {
                        if (className == "product") {
                            window.location = window.location.href;
                        } else
                            window.location.href = myaccountURL;
                    }
                } else {
                    alert("invalid user/password");
                    // console.log(data);
                }
            }
        });
        return false;
    });

    $(document).load(function() {
        $('#SlideModal').modal('show');
    });

});




//wait for ajax
$(document).on({
    ajaxStart: function() { if (disableAjaxLoadUI == false) { $body.addClass("loading"); } },
    ajaxStop: function() {
        $body.removeClass("loading");
        disableAjaxLoadUI = false;
    }
});

function addTitleTextRentCal() {
    $('.ui-datepicker td.unavailable>a').attr('title', 'product not available.');
    $('.ui-datepicker td.ui-state-disabled.closed>span').attr('title', 'order start and end date cannot be on this day');
    $('.ui-datepicker td.waiting>a').attr('title', 'order is in waiting list.');
    var date1 = $.datepicker.parseDate('dd/M/yy', $("#date1").val());
    var date2 = $.datepicker.parseDate('dd/M/yy', $("#date2").val());
    if (date1) {
        if (!date2) {
            $('#days').val(1);
            $('#ldays').text(1);
        } else {
            days = ((date2 - date1) / (1000 * 60 * 60 * 24)) + 1;
            $('#days').val(days);
            $('#ldays').text(days);
        }
    }
}

function checkWaiting() {
    if (waitingList) {
        $('#waitingList').modal('show');
        return false;
    }
    return true;
}

$(document).ready(function() {

    $("#edit").click(function() {
        $('#displaypersonaldetails').addClass('hide');
        $('#editpersondetails').removeClass("hide");
    });

    $("#editcontact").click(function() {
        $('#displaycontactdetails').addClass('hide');
        $('#editcontactdetails').removeClass("hide");
    });

    $("#editoffice").click(function() {
        $('#displayofficedetails').addClass('hide');
        $('#editofficedetails').removeClass("hide");
    });

    $("#editcompinfo").click(function() {
        $('#displaycompinfodetails').addClass('hide');
        $('#editcompinfodetails').removeClass("hide");
    });

    $("#companyChoice1").click(function() {
        $('#companyinput').addClass("hide");
    });
    $("#companyChoice2").click(function() {
        $('#companyinput').removeClass("hide");
    });

    $("#editpan").click(function() {
        $('#pancarddetails').addClass('hide');
        $('#editpandetails').removeClass("hide");
    });
    $("#editdob").click(function() {
        $('#dobdetails').addClass('hide');
        $('#editdobdetails').removeClass("hide");
    });
    $("#dob").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        yearRange: "-100:+0",
        changeYear: true
    });
    $('#date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });

    //discount form ajax sumbit
    $('#formDiscount #discountAction').on("submit", function() {
        $('#formDiscount input[name=discountAction]').val($(this).attr('value'));
    });

    formDiscount = $('#formDiscount');
    formDiscount.on("submit", function(ev) {
        ev.preventDefault();
        $.ajax({
            type: formDiscount.attr('method'),
            url: formDiscount.attr('action'),
            data: formDiscount.serialize(),
            success: function(data) {
                var data = jQuery.parseJSON(data);
                if (data.status == true) {
                    window.location = window.location.href;
                } else {
                    //$('#couponError').html(data.message);
                    alert(data.message);


                }
            }
        });
        return false;
    });

    $('.couponError').hide();
    formHeaderDiscount = $('#promocodeForm');
    formHeaderDiscount.submit(function(ev) {
        ev.preventDefault();
        $.ajax({
            type: formHeaderDiscount.attr('method'),
            url: formHeaderDiscount.attr('action'),
            data: formHeaderDiscount.serialize(),
            success: function(data) {
                var data = jQuery.parseJSON(data);
                if (data.status) {
                    alert('Discount Successfully Applied');
                    window.location = window.location.href;
                } else {
                    $('#couponError').html(data.message);
                    $('.couponError').show();
                }
            }
        });
        return false;
    });
});

function showHide(showDiv, hideDiv) {
    $('#' + showDiv).removeClass('hide');
    $('#' + hideDiv).addClass('hide');
}